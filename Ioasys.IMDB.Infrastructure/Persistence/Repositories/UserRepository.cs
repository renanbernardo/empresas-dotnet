﻿using Ioasys.IMDB.Core.Entities;
using Ioasys.IMDB.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Ioasys.IMDB.Infrastructure.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ImdbDbContext _dbContext;
        public UserRepository(ImdbDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> GetAsync(int id)
        {
            return await _dbContext
                .Users
                .SingleOrDefaultAsync(u => u.Id == id);
        }

        public async Task InsertAsync(User user)
        {
            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<User> GetUserAsync(string email, string hashPassword)
        {
            return await _dbContext
                .Users
                .SingleOrDefaultAsync(u => u.Email == email && u.Password == hashPassword);
        }
    }
}
