﻿using Ioasys.IMDB.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.IMDB.Infrastructure.Persistence.Configurations
{
    public class VoteConfigurations : IEntityTypeConfiguration<Vote>
    {
        public void Configure(EntityTypeBuilder<Vote> builder)
        {
            builder
                .HasKey(prop => new { prop.IdMovie, prop.IdUser });

            builder
                .HasOne(prop => prop.Movie)
                .WithMany(prop => prop.Scores)
                .HasForeignKey(prop => prop.IdMovie);

            builder
                .HasOne(prop => prop.User)
                .WithMany(prop => prop.Votes)
                .HasForeignKey(prop => prop.IdUser);
        }

    }
}
