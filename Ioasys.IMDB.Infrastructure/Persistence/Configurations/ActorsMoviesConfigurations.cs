﻿using Ioasys.IMDB.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.IMDB.Infrastructure.Persistence.Configurations
{
    public class ActorsMoviesConfigurations : IEntityTypeConfiguration<ActorMovie>
    {
        public void Configure(EntityTypeBuilder<ActorMovie> builder)
        {
            builder
                .HasKey(prop => new { prop.IdActor, prop.IdMovie });

            builder
                .HasOne(prop => prop.Movie)
                .WithMany(prop => prop.Cast)
                .HasForeignKey(prop => prop.IdMovie);

            builder
                .HasOne(prop => prop.Actor)
                .WithMany(prop => prop.Filmography)
                .HasForeignKey(prop => prop.IdActor);
        }
    }
}
