﻿using Ioasys.IMDB.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.IMDB.Infrastructure.Persistence.Configurations
{
    public class UserConfigurations : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasKey(prop => prop.Id);

            builder
                .Property(prop => prop.Email)
                .IsRequired()
                .HasColumnType("varchar(255)");

            builder
                .Property(prop => prop.Password)
                .IsRequired()
                .HasColumnType("varchar(100)");

        }
    }
}
