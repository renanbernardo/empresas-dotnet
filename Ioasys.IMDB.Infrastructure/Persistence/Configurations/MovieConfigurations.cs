﻿using Ioasys.IMDB.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.IMDB.Infrastructure.Persistence.Configurations
{
    public class MovieConfigurations : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder
                .HasKey(prop => prop.Id);

            builder
                .HasMany(prop => prop.Scores)
                .WithOne(prop => prop.Movie)
                .HasForeignKey(prop => prop.IdMovie)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
