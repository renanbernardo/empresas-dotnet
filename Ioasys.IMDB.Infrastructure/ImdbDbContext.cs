﻿using Ioasys.IMDB.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Ioasys.IMDB.Infrastructure
{
    public class ImdbDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<ActorMovie> ActorsMovies { get; set; }

        public ImdbDbContext(DbContextOptions<ImdbDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
