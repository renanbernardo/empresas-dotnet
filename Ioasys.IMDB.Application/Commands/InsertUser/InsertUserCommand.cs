﻿using Ioasys.IMDB.Core.Enums;
using MediatR;
using System;

namespace Ioasys.IMDB.Application.Commands.InsertUser
{
    public class InsertUserCommand : IRequest<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public ERole Role { get; set; }
        public string Password { get; set; }
    }
}
