﻿using Ioasys.IMDB.Core.Entities;
using Ioasys.IMDB.Core.Repositories;
using Ioasys.IMDB.Core.Services;
using Ioasys.IMDB.Infrastructure.Persistence;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDB.Application.Commands.InsertUser
{
    public class InsertUserCommandHandler : IRequestHandler<InsertUserCommand, int>
    {
        private readonly IUserRepository _repository;
        private readonly IAuthService _authService;
        public InsertUserCommandHandler(IUserRepository repository, IAuthService authService)
        {
            _repository = repository;
            _authService = authService;
        }

        public async Task<int> Handle(InsertUserCommand command, CancellationToken cancellationToken)
        {
            var hashPassword = _authService.GenerateHash(command.Password);
            var user = new User(command.FirstName, command.LastName, command.BirthDate, command.Email, hashPassword, command.Role);

            await _repository.InsertAsync(user);

            return user.Id;
        }
    }
}
