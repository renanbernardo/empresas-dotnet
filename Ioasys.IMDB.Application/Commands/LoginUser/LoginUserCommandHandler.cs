﻿using Ioasys.IMDB.Application.ViewModels;
using Ioasys.IMDB.Core.Repositories;
using Ioasys.IMDB.Core.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDB.Application.Commands.LoginUser
{
    public class LoginUserCommandHandler : IRequestHandler<LoginUserCommand, LoginUserViewModel>
    {
        private readonly IAuthService _authService;
        private readonly IUserRepository _userRepository;

        public LoginUserCommandHandler(IAuthService authService, IUserRepository userRepository)
        {
            _authService = authService;
            _userRepository = userRepository;
        }

        public async Task<LoginUserViewModel> Handle(LoginUserCommand command, CancellationToken cancellationToken)
        {
            string hashPassword = _authService.GenerateHash(command.Password);
            var user = await _userRepository.GetUserAsync(command.Email, hashPassword);

            if (user == null)
            {
                return null;
            }

            var token = _authService.GenerateToken(user.Email, user.Role.ToString());

            return new LoginUserViewModel(user.Email, token);
        }
    }
}
