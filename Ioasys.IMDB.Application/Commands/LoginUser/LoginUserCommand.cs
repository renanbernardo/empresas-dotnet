﻿using Ioasys.IMDB.Application.ViewModels;
using MediatR;

namespace Ioasys.IMDB.Application.Commands.LoginUser
{
    public class LoginUserCommand : IRequest<LoginUserViewModel>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
