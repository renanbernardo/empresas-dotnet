﻿using System;

namespace Ioasys.IMDB.Application.InputModels
{
    public class InsertMovieInputModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int Length { get; set; }
        public int IdDirector { get; set; }
        public int IdGenre { get; set; }

    }
}
