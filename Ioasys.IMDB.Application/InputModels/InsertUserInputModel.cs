﻿using Ioasys.IMDB.Core.Enums;
using System;

namespace Ioasys.IMDB.Application.InputModels
{
    public class InsertUserInputModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime dateTime { get; set; }
        public string Email { get; set; }
        public ERole Role { get; set; }
    }
}
