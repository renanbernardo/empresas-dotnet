﻿using Ioasys.IMDB.Application.ViewModels;
using Ioasys.IMDB.Core.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDB.Application.Queries.GetUser
{
    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserViewModel>
    {
        private readonly IUserRepository _repository;
        public GetUserQueryHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<UserViewModel> Handle(GetUserQuery query, CancellationToken cancellationToken)
        {
            var user = await _repository.GetAsync(query.Id);

            if (user == null)
            {
                return null;
            }

            return new UserViewModel(user.Name, user.LastName, user.Email);
        }
    }
}
