﻿using Ioasys.IMDB.Application.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.IMDB.Application.Queries.GetUser
{
    public class GetUserQuery : IRequest<UserViewModel>
    {
        public int Id { get; private set; }

        public GetUserQuery(int id)
        {
            Id = id;
        }
    }
}
