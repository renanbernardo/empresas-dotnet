﻿using System;

namespace Ioasys.IMDB.Application.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel(string firstName, string lastName, string email)
        {
            FullName = $"{firstName} {lastName}";
            Email = email;
        }

        public string FullName { get; set; }
        public string Email { get; set; }

    }
}
