﻿
namespace Ioasys.IMDB.Core.Enums
{
    public enum EScore
    {
        Terrible,
        Bad,
        Ok,
        Good,
        Excellent
    }
}
