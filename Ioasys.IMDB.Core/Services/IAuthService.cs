﻿namespace Ioasys.IMDB.Core.Services
{
    public interface IAuthService
    {
        string GenerateToken(string email, string role);
        string GenerateHash(string password);
    }
}
