﻿using Ioasys.IMDB.Core.Enums;
using Flunt.Notifications;
using Flunt.Validations;

namespace Ioasys.IMDB.Core.Entities
{
    public class Vote : Notifiable<Notification>
    {
        public Vote(int idMovie, int idUser, EScore score)
        {
            IdMovie = idMovie;
            IdUser = idUser;
            Score = score;

            AddNotifications(new Contract<Vote>()
                .Requires()
                .IsBetween((int)Score, 0, 4, "Score", "O voto deve estar entre 0 e 4.")
                );
        }

        public int IdMovie { get; private set; }
        public Movie Movie { get; private set; }
        public int IdUser { get; private set; }
        public User User { get; private set; }
        public EScore Score { get; private set; }
    }
}
