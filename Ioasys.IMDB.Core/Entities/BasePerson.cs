﻿using System;
using Flunt.Validations;

namespace Ioasys.IMDB.Core.Entities
{
    public abstract class BasePerson : BaseEntity
    {
        protected BasePerson(
            string firstName, 
            string lastName, 
            DateTime birthDate
            ) 
            : base(firstName)
        {
            LastName = lastName;
            BirthDate = birthDate;

            AddNotifications(new Contract<BasePerson>()
                .Requires()
                .IsLowerThan(LastName, 3, "LastName", "O sobrenome deve conter pelo menos 3 caracteres")
                );
        }

        public string LastName { get; private set; }
        public DateTime BirthDate { get; private set; }
        
    }
}
