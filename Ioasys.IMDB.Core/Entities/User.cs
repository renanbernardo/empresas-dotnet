﻿using Ioasys.IMDB.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Flunt.Validations;

namespace Ioasys.IMDB.Core.Entities
{
    public class User : BasePerson
    {
        private IList<Vote> _votes;

        public User(
            string firstName, 
            string lastName, 
            DateTime birthDate, 
            string email, 
            string password, 
            ERole role
            ) : base(
                firstName, 
                lastName, 
                birthDate
                )
        {
            Email = email;
            Password = password;
            Role = role;
            Active = true;
            _votes = new List<Vote>();

            AddNotifications(new Contract<User>()
               .Requires()
               .IsNotEmail(Email, "Email", "O email digitado não é válido")
               .IsLowerThan(Password, 8, "Password", "A senha deve conter pelo menos 8 caracteres")
               );
        }

        public string Email { get; private set; }
        public string Password { get; private set; }
        public ERole Role{ get; private set; }
        public bool Active { get; private set; }
        public IReadOnlyCollection<Vote> Votes { get { return _votes.ToArray(); } }

        public void Deactivate()
            => Active = false;

        public void Activate()
            => Active = true;

    }
}
