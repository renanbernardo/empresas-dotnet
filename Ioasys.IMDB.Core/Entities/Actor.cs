﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ioasys.IMDB.Core.Entities
{
    public class Actor : BasePerson
    {
        private IList<ActorMovie> _filmography;

        public Actor(string firstName, string lastName, DateTime birthDate) : base(firstName, lastName, birthDate)
        {
            _filmography = new List<ActorMovie>();
        }

        public IReadOnlyCollection<ActorMovie> Filmography { get { return _filmography.ToArray(); } }
    }
}
