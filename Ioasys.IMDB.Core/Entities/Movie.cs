﻿using System;
using System.Collections.Generic;
using System.Linq;
using Flunt.Validations;

namespace Ioasys.IMDB.Core.Entities
{
    public class Movie : BaseEntity
    {
        private IList<ActorMovie> _actors;
        private IList<Vote> _scores;

        public Movie(
            string title, 
            DateTime releaseDate, 
            int length, 
            Director director, 
            Genre genre
            ) : base(
                title
                )
        {
            ReleaseDate = releaseDate;
            Length = length;
            Director = director;
            Genre = genre;
            _actors = new List<ActorMovie>();
            _scores = new List<Vote>();

            AddNotifications(new Contract<Movie>()
               .Requires()
               .IsLowerOrEqualsThan(Length, 1, "Length", "Duração inválida")
               .IsLowerThan(ReleaseDate, new DateTime(1878, 06, 19), "ReleaseDate", "Data de lançamento não permitida")
               );
        }

        public DateTime ReleaseDate { get; private set; }
        public int Length { get; private set; }
        public IReadOnlyCollection<ActorMovie> Cast { get { return _actors.ToArray(); } }
        public Director Director { get; private set; }
        public Genre Genre { get; private set; }
        public IReadOnlyCollection<Vote> Scores { get { return _scores.ToArray();  } }

        public void AddActor(Actor actor)
        {
            _actors.Add(new ActorMovie(actor.Id, Id));
        }

        public void AddScore(Vote vote)
        {
            _scores.Add(vote);
        }

        public double Rating()
        {
            return _scores.Average(x => (int)x.Score);
        }
    }
}
