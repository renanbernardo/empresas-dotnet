﻿using System;

namespace Ioasys.IMDB.Core.Entities
{
    public class Director : BasePerson
    {
        public Director(string firstName, string lastName, DateTime birthDate) : base(firstName, lastName, birthDate)
        {
        }
    }
}
