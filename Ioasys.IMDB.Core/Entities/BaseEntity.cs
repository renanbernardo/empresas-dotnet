﻿using System;
using Flunt.Notifications;
using Flunt.Validations;

namespace Ioasys.IMDB.Core.Entities
{
    public abstract class BaseEntity : Notifiable<Notification>
    {
        protected BaseEntity(string name)
        {
            Name = name;
            CreatedAt = DateTime.Now;

            AddNotifications(new Contract<BaseEntity>()
                .Requires()
                .IsLowerThan(Name, 3, "Name", "O nome deve conter pelo menos 3 caracteres")
                );  
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime CreatedAt { get; private set; }
    }
}
