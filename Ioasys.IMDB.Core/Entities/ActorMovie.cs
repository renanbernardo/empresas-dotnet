﻿namespace Ioasys.IMDB.Core.Entities
{
    public class ActorMovie
    {
        public ActorMovie(int idActor, int idMovie)
        {
            IdActor = idActor;
            IdMovie = idMovie;
        }

        public int IdActor { get; private set; }
        public Actor Actor { get; set; }
        public int IdMovie { get; private set; }
        public Movie Movie { get; set; }
    }
}
