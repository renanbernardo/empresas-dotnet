﻿using Ioasys.IMDB.Core.Entities;
using System.Threading.Tasks;

namespace Ioasys.IMDB.Core.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetAsync(int id);
        Task InsertAsync(User user);
        Task<User> GetUserAsync(string email, string hashPassword);
    }
}
